#!/home/tls/anaconda3/envs/lidar/bin/python
"""
Functions for calculating the plot average vertical plant profiles (Calders et al., 2014)
Generate PAVD average estimates from a number of TLS scans

Kim Calders
John Armston

Input are the individual PAVD.csv files generated with pylidar_canopy
The average and confidence interval of the Pgap is used to calculate the plot PAVD profile
"""

import csv
import glob
import os
import math
from pylidar.toolbox.canopy import pavd_calders2014
import optparse
from numpy import genfromtxt
import numpy as np

def average_pgap():
	"""
	Read the PgapData data from individual scans and average over the number of scans
	"""
	files=glob.glob('*.csv')
	print("Number of scan locations in plot is %i" % len(files))

	#getheader
	reader = csv.reader(open(files[0]), delimiter=',')
	header=next(reader)
	#get mid zeniths
	zenith=np.array([])
	for i in range(1,len(header)-5):
		zenith=np.append(zenith,(float(header[i].split("vz")[1])/100))
	zenith=np.radians(zenith) #convert to radians
	zenithbinsize=zenith[1]-zenith[0]
	    
	#add up all data - this skips the header
	avgdata = genfromtxt(files[0], delimiter=',')
	for f in files[1:]:
		avgdata += genfromtxt(f, delimiter=',')
	#calculate the average
	avgdata /= len(files)
	#calculate the standard deviation
	sddata = np.zeros((len(avgdata), len(avgdata[0])))
	sddata[:,0]=avgdata[:,0] #populating height column
	for f in files:
		tmp = genfromtxt(f, delimiter=',')
		for i in range(0,len(avgdata)):
			for j in range(1,len(avgdata[0])):
				sddata[i,j] += (tmp[i,j]-avgdata[i,j])**2 #this will get the sum of the squared residuals
	for i in range(0,len(avgdata)):
		for j in range(1,len(avgdata[0])):
			sddata[i,j] = math.sqrt(sddata[i,j]/len(files))

#	np.savetxt("data_avg.csv",avgdata,fmt="%s",delimiter=",")
#	np.savetxt("data_sd.csv",sddata,fmt="%s",delimiter=",")
	height=avgdata[:,0]
	heightbinsize=height[1]-height[0]

	PAVD_l=pgapz=avgdata[:,-1]-1.96*sddata[:,-1]
	PAVD_l[PAVD_l < 0] = 0 #PAVD cannot be negative
	PAVD_u=pgapz=avgdata[:,-1]+1.96*sddata[:,-1]
	PAVD_u[PAVD_u < 0] = 0 #PAVD cannot be negative

	pgapz=avgdata[:,1:-5]
	pgapz_sd=sddata[:,1:-5]
	#lower 95%
	pgapz_l=pgapz-1.96*pgapz_sd
	for j in range(0,len(pgapz_l[0])):
		canopy_exit=False
		for i in range(0,len(pgapz_l)):
			if pgapz_l[i,j] < 0.0 or canopy_exit == True:
				canopy_exit=True
				pgapz_l[i,j] = 0.00000001 #gap fraction cannot be smaller than 0 & once it reaches this > all following heightbins also to this value
			if i > 0 and pgapz_l[i,j] > pgapz_l[i-1,j]:
				pgapz_l[i,j]=pgapz_l[i-1,j] #gap fraction cannot increase
	#upper 95%
	pgapz_u=pgapz+1.96*pgapz_sd
	for j in range(0,len(pgapz_u[0])):
		for i in range(0,len(pgapz_u)):
			if pgapz_u[i,j] > 1.0:
		    		pgapz_u[i,j] = 1 #gap fraction cannot be more than 1
			if i > 0 and pgapz_u[i,j] > pgapz_u[i-1,j]:
				pgapz_u[i,j]=pgapz_u[i-1,j] #gap fraction cannot increase

	pgapz=np.transpose(pgapz) #transpose the matrix
	pgapz_u=np.transpose(pgapz_u)
	pgapz_l=np.transpose(pgapz_l)

	return pgapz,pgapz_u,pgapz_l,zenith,zenithbinsize,height,heightbinsize,PAVD_l,PAVD_u


def generateVPP(pgapz,whichvalue,zenith,zenithbinsize,height,heightbinsize):
	lpp_pai,lpp_pavd,lpp_mla = pavd_calders2014.calcLinearPlantProfiles(height, heightbinsize, zenith, pgapz)
	sapp_pai,sapp_pavd = pavd_calders2014.calcSolidAnglePlantProfiles(zenith, pgapz, heightbinsize,zenithbinsize)
	outFile = cmdargs.plotname + "_" + whichvalue + "_FoliageProfile.csv"
	pavd_calders2014.writeProfiles(outFile, np.degrees(zenith), height, pgapz, lpp_pai, lpp_pavd, lpp_mla, sapp_pai, sapp_pavd)



def main(cmdargs):
	print("I am assuming that all individual profiles are created with the same settings (e.g. max height, bin)...") #build in check later
	pgapz,pgapz_u,pgapz_l,zenith,zenithbinsize,height,heightbinsize,PAVD_l,PAVD_u=average_pgap()

	generateVPP(pgapz,"average",zenith,zenithbinsize,height,heightbinsize)

	lower=np.stack((height,PAVD_l),axis=-1)
	np.savetxt(cmdargs.plotname + "_lower95.csv",lower,header="height, juppPAVD",fmt="%s",delimiter=",")
	upper=np.stack((height,PAVD_u),axis=-1)
	np.savetxt(cmdargs.plotname + "_upper95.csv",upper,header="height, juppPAVD",fmt="%s",delimiter=",")
	
#	generateVPP(pgapz_u,"upper95",zenith,zenithbinsize,height,heightbinsize)	
#	generateVPP(pgapz_l,"lower95",zenith,zenithbinsize,height,heightbinsize)	


# Command arguments
class CmdArgs:
  def __init__(self):
    p = optparse.OptionParser()
    p.add_option("--plotname", dest="plotname", type="string", default="PLOTNAME", help="The name of the field plot")
    (options, args) = p.parse_args()
    self.__dict__.update(options.__dict__)

# Run the script
if __name__ == "__main__":
    cmdargs = CmdArgs()
    main(cmdargs)

